# Ansible Role for Swap file
This is a role for Ansible to manage a swap file

## Variables
|        Name         |       Description                     | Optional |                 Default                |
| ------------------- | ------------------------------------- | -------- | -------------------------------------- |
| `swap_file_size_mb` | The size of your swap file in MB      |     ✓    |   `{{ ansible_memory_mb.real.total }}` |
| `swap_file_path`    | Path to where you want your swap file |     ✓    |   `/swap`                              |

## Usage
This is designed to work with Ansible Galaxy. [Follow the guidance](https://docs.ansible.com/ansible/latest/reference_appendices/galaxy.html) to make use of this role.

You can use this role in your site playbook like so:

```yaml
- hosts: all
  remote_user: root
  roles:
    - role: swap
      vars:
         swap_file_size_mb: 2048
```
